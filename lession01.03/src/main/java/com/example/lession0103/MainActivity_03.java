package com.example.lession0103;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity_03 extends AppCompatActivity {
    TextView mToysListTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_03);

        mToysListTextView = (TextView) findViewById(R.id.tv_toy_names);
        String[] toyNames = ToyBox.getToyNames();
        for (String toyName : toyNames) {
            mToysListTextView.append(toyName + "\n\n\n");
        }
    }
}
