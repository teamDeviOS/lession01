# README #
Lession 01:
01:
<!--TODO (1) Change the ConstraintLayout to a FrameLayout-->
<!--TODO (2) Remove all attributes with the word constraint in them-->
<!--TODO (3) Remove the default text-->
<!--TODO (4) Give the TextView 16dp of padding-->
<!--TODO (5) Set the text size to 20sp-->
<!--TODO (6) Remove the line that declares the id, we don't need it-->

02:
 // TODO (1) Declare a TextView variable called mToysListTextView
 // TODO (2) Add an id to the TextView with the value @+id/tv_toy_names
 // TODO (3) Use findViewById to get a reference to the TextView from the layout
 // TODO (4) Use the static ToyBox.getToyNames method and store the names in a String array
 // TODO (5) Loop through each toy and append the name to the TextView (add \n for spacing)
 
 03:
     <!--TODO (1) Add a ScrollView around the TextView so you can scroll through the list of toys-->
    <!--TODO (2) Make the width of the ScrollView match_parent and the height wrap_content-->

04:
